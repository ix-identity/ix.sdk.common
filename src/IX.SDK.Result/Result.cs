﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.Serialization;
using ProtoBuf;

namespace IX.SDK.Result
{
    [ProtoContract]
    public class Result : IResult
    {
        [ProtoMember(1)]public bool Success { get; }
        [ProtoMember(2)]public Error[] Errors { get; }
        [ProtoMember(3)]public bool Failure => !Success;
        
        protected Result(bool success, Error error = null)
        {
            Success = success;
            if (error != null)
            {
                Errors = new[] { error };
            }
        }

        protected Result(bool success, Error[] errors)
        {
            Success = success;
            Errors = errors;
        }

        public static Result Fail(string message = null)
        {
            var error = string.IsNullOrEmpty(message) ? null : new Error(message);
            return new Result(false, error);
        }

        public static Result Fail(Error[] errors)
        {
            return new Result(false, errors);
        }

        public static Result Fail(string key, string message)
        {
            var error = string.IsNullOrEmpty(message) || string.IsNullOrEmpty(key) ? null : new Error(key, message);
            return new Result(false, error);
        }

        public static Result<T> Fail<T>(string message = null)
        {
            var error = string.IsNullOrEmpty(message) ? null : new Error(message);
            return new Result<T>(default(T), false, error);
        }

        public static Result<T> Fail<T>(Error error)
        {
            return new Result<T>(default(T), false, error);
        }

        public static Result<T> Fail<T>(Error[] errors)
        {
            return new Result<T>(default(T), false, errors);
        }

        public static Result<T> Fail<T>(string key, string message)
        {
            var error = string.IsNullOrEmpty(message) || string.IsNullOrEmpty(key) ? null : new Error(key, message);
            return new Result<T>(default, false, error);
        }

        public static Result Ok()
        {
            return new Result(success: true);
        }

        public static Result<T> Ok<T>(T value)
        {
            return new Result<T>(value, true);
        }

        public static Result Combine(params Result[] results)
        {
            foreach (var result in results)
            {
                if (result.Failure)
                {
                    return result;
                }
            }

            return Ok();
        }

        public static Result Concat(params Result[] results)
        {
            var errors = results
                .Where(x => x.Failure)
                .Aggregate(new List<Error>(), (x, y) =>
                {
                    x.AddRange(y.Errors);
                    return x;
                });

            return errors.Any()
                ? Fail(errors.ToArray())
                : Ok();
        }

        public Result<T> ToGenericResult<T>()
        {
            var result = new Result<T>(default, Success, Errors);
            return result;
        }
    }
    
    [ProtoContract]
    [ProtoInclude(1000, nameof(Result))]
    public class Result<T> : Result
    {
        [ProtoMember(4)]public T Data { get; }

        protected internal Result([AllowNull] T data, bool success, Error error = null)
            : base(success, error)
        {
            Data = data;
        }

        protected internal Result([AllowNull] T data, bool success, Error[] errors)
            : base(success, errors)
        {
            Data = data;
        }

        public new Result<TU> ToGenericResult<TU>()
        {
            var type = typeof(TU);
            var underlyingType = Nullable.GetUnderlyingType(type);

            return underlyingType != null
                ? new Result<TU>(Data == null ? default : (TU)Convert.ChangeType(Data, underlyingType), Success, Errors)
                : new Result<TU>((TU)Convert.ChangeType(Data, type), Success, Errors);
        }
    }
}