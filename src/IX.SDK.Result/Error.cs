﻿using System.Runtime.Serialization;

namespace IX.SDK.Result
{
    [DataContract]
    public class Error
    {
        [DataMember(Order = 1)]public string Key { get; }
        [DataMember(Order = 2)]public string Message { get; }

        public Error(string message)
        {
            Key = "rule_error";
            Message = message;
        }

        public Error(string key, string message)
        {
            Key = key;
            Message = message;
        }
    }
}