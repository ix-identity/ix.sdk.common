﻿using IX.SDK.PasswordHashing.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace IX.SDK.PasswordHashing
{
    public class SaltGenerateService : ISaltGenerateService
    {
        private readonly RandomNumberGenerator _generator;
        private readonly int _minSaltSize = 8;

        public SaltGenerateService(RandomNumberGenerator generator)
        {
            _generator = generator ?? throw new ArgumentNullException(nameof(generator));
        }

        public byte[] Generate(int saltSize)
        {
            byte[] salt = new byte[saltSize];
            _generator.GetBytes(salt);
            return salt;
        }

        public byte[] ModifyForMinSize(byte[] salt)
        {
            if (salt.Length < _minSaltSize)
            {
                salt = salt.Concat(salt).ToArray();
                return ModifyForMinSize(salt);
            }

            return salt;
        }
    }
}
