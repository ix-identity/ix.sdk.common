﻿using System;
using System.Text;

namespace IX.SDK.PasswordHashing
{
    internal static class ByteHashExtension
    {
        internal static string HashToString(this byte[] hash)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }
    }
}
