using IX.SDK.PasswordHashing;
using System;
using System.Security.Cryptography;
using Xunit;

namespace PasswordHashingTests
{
    public class PasswordHashingServiceTests
    {
        /* TODO
         - ���� �� ������������� ����������� ���� � ������ ������
         - ���� �� ��������� �������� ����, ������ � ������ �������� �� ���������� ���� �� sha256
         - ���� �� ����������� ������� �����, �������� �������� �����
            - ������� ����������� ��������� ����� � �������
         - DependencyInjection ��������� ��� ���� ����������� 

         */
        [Fact]
        public void HashNotEmptyTest()
        {
            var service = new PasswordHashingService(new PasswordHashingServiceOptions(), new SaltGenerateService(new RNGCryptoServiceProvider()));
            var result = service.Hash("����");

            Assert.NotEqual(string.Empty, result.Salt);
            Assert.NotEqual(string.Empty, result.PasswordHash);
        }

        [Fact]
        public void HashEqualHashTest()
        {
            var service = new PasswordHashingService(new PasswordHashingServiceOptions(), new SaltGenerateService(new RNGCryptoServiceProvider()));
            var hash1 = service.Hash("����123!", "salt");
            var hash2 = service.Hash("����123!", "salt");

            Assert.Equal(hash1, hash2);
        }

    }
}
