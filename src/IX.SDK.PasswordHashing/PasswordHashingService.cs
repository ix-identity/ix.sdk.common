﻿using IX.SDK.PasswordHashing.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace IX.SDK.PasswordHashing
{
    public class PasswordHashingService : IPasswordHashingService
    {
        private readonly int _saltSize;                     // size in bytes
        private readonly int _hashSize;                     // size in bytes
        private readonly int _iterations;                   // number of pbkdf2 iterations
        private readonly HashAlgorithmName _hashAlgorithm;  // hash algorithm name

        private readonly ISaltGenerateService _saltGenerateService;

        public PasswordHashingService(PasswordHashingServiceOptions options, ISaltGenerateService saltGenerateService)
        {
            _saltSize = options.SaltSize;
            _hashSize = options.HashSize;
            _iterations = options.Iterations;
            _hashAlgorithm = options.HashAlgorithm;

            _saltGenerateService = saltGenerateService ?? throw new ArgumentNullException(nameof(saltGenerateService));
        }

        public (string Salt, string PasswordHash) Hash(string password)
        {
            var saltBytes = _saltGenerateService.Generate(_saltSize);

            var hash = GetPbkdf2Hash(password, saltBytes);

            return (saltBytes.HashToString(), hash.HashToString());
        }

        public string Hash(string password, string salt)
        {
            var saltBytes = Encoding.UTF8.GetBytes(salt);
            var modifySaltBytes = _saltGenerateService.ModifyForMinSize(saltBytes);

            var hash = GetPbkdf2Hash(password, modifySaltBytes);

            return hash.HashToString();
        }

        public string Hash(string password, string salt1, string salt2)
        {
            return Hash(password, salt1 + salt2);
        }

        private byte[] GetPbkdf2Hash(string password, byte[] saltBytes)
        {
            Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(password, saltBytes, _iterations, _hashAlgorithm);
            return pbkdf2.GetBytes(_hashSize);
        }
    }
}
