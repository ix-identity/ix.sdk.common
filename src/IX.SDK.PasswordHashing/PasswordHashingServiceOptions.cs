﻿using System.Security.Cryptography;

namespace IX.SDK.PasswordHashing
{
    public class PasswordHashingServiceOptions
    {
        /// <summary>
        /// The size of the generated salt in bytes. A salt will prevent the lookup of hash values in rainbow tables
        /// </summary>
        public int SaltSize { get; set; } = 24;

        /// <summary>
        /// The size of password hash in bytes
        /// </summary>
        public int HashSize { get; set; } = 24;

        /// <summary>
        /// A high number of iterations will slow the algorithm down, which makes password cracking a lot harder. 
        /// </summary>
        public int Iterations { get; set; } = 100_000;

        /// <summary>
        /// Hash algorithm
        /// </summary>
        public HashAlgorithmName HashAlgorithm { get; set; } = HashAlgorithmName.SHA256;
    }
}
