﻿using System;
using System.Threading.Tasks;

namespace IX.SDK.Result.Extensions
{
    public static class ExpressionExtension
    {
        
        /// <summary>
        /// В случае успеха, выполняет переданный метод как колбэк с данными результата, как параметр
        /// </summary>
        public static Result OnSuccess<T>(this Result<T> result, Action<T> action)
        {
            if (result.Failure)
            {
                return result;
            }

            action(result.Data);

            return Result.Ok();
        }

        /// <summary>
        /// В случае успеха, выполняет переданный метод как колбэк возвращающий
        /// </summary>
        public static Result<T> OnSuccess<T>(this Result result, Func<T> func)
        {
            return result.Failure ? Result.Fail<T>(result.Errors) : Result.Ok(func());
        }

        /// <summary>
        /// Выполнит метод и вернет результат выполнения в случае успеха, иначе вернет исходный результат
        /// </summary>
        public static Result<T> OnSuccess<T>(this Result result, Func<Result<T>> func)
        {
            return result.Failure ? Result.Fail<T>(result.Errors) : func();
        }

        /// <summary>
        /// Выполнит метод и вернет результат выполнения в случае успеха, иначе вернет исходный результат
        /// </summary>
        public static Result OnSuccess<T>(this Result<T> result, Func<T, Result> func)
        {
            return result.Failure ? result : func(result.Data);
        }

        /// <summary>
        /// Выполнит метод и вернет результат выполнения в случае успеха, иначе вернет исходный результат
        /// </summary>
        public static async Task<Result> OnSuccessAsync<T>(this Task<Result<T>> source, Func<T, Task<Result>> func)
        {
            var result = await source;
            if (result.Failure)
            {
                return result;
            }

            return await func(result.Data);
        }

        /// <summary>
        /// Выполнит метод и вернет результат выполнения в случае успеха, иначе вернет исходный результат
        /// </summary>
        public static async Task<Result<T>> OnSuccessAsync<T>(this Task<Result> source, Func<Task<Result<T>>> func)
        {
            var result = await source;
            if (result.Failure)
            {
                return Result.Fail<T>(result.Errors);
            }

            return await func();
        }

        /// <summary>
        /// Выполнит метод и вернет результат выполнения в случае успеха, иначе вернет исходный результат
        /// </summary>
        public static async Task<Result<TNext>> OnSuccessAsync<TPrevious, TNext>(this Task<Result<TPrevious>> source, Func<TPrevious, Task<Result<TNext>>> func)
        {
            var result = await source;
            if (result.Failure)
            {
                return Result.Fail<TNext>(result.Errors);
            }

            return await func(result.Data);
        }
    }
}