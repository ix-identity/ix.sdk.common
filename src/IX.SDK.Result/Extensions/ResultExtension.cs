﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace IX.SDK.Result.Extensions
{
    public static class ResultExtension
    {
        /// <summary>
        /// Возвращает текущий результат в случае провала, или следующий в случае успеха
        /// </summary>
        public static Result OnSuccess(this Result result, Func<Result> func)
        {
            return result.Failure ? result : func();
        }

        /// <summary>
        /// В случае успеха, выполняет переданный метод как колбэк
        /// </summary>
        public static Result OnSuccess(this Result result, Action action)
        {
            if (result.Failure)
            {
                return result;
            }

            action();

            return Result.Ok();
        }

        /// <summary>
        /// Выполнит метод и вернет результат выполнения в случае успеха, иначе вернет исходный результат
        /// </summary>
        public static async Task<Result> OnSuccess(this Result result, Func<Task<Result>> func)
        {
            if (result.Failure)
            {
                return result;
            }

            return await func();
        }

        /// <summary>
        /// Выполнит метод и вернет результат выполнения в случае успеха, иначе вернет исходный результат
        /// </summary>
        public static async Task<Result> OnSuccess(this Task<Result> source, Func<Task<Result>> func)
        {
            var res = await source;
            if (res.Failure)
            {
                return res;
            }

            return await func();
        }

        /// <summary>
        /// Выполнит метод в случае неудачи и вернет исходный результат
        /// </summary>
        public static Result OnFailure(this Result result, Action action)
        {
            if (result.Failure)
            {
                action();
            }

            return result;
        }

        /// <summary>
        /// Выполнит метод в случае неудачи и вернет исходный результат
        /// </summary>
        public static async Task<Result> OnFailure(this Task<Result> source, Func<Task<Result>> func)
        {
            var res = await source;
            if (res.Failure)
            {
                await func();
            }

            return res;
        }

        /// <summary>
        /// Выполняет экшн вне зависимости от результата
        /// </summary>
        public static Result OnBoth(this Result result, Action<Result> action)
        {
            action(result);

            return result;
        }

        /// <summary>
        /// Выполняет метод вне зависимости от результата
        /// </summary>
        public static T OnBoth<T>(this Result result, Func<Result, T> func)
        {
            return func(result);
        }

        /// <summary>
        /// В случае успеха вернет результат выполнения метода success, в противном случае результат метода fail
        /// </summary>
        public static T Return<T>(this Result result, Func<T> success, Func<Result, T> fail)
        {
            return result.Failure ? fail(result) : success();
        }

        /// <summary>
        /// В случае успеха вернет результат выполнения метода success, в противном случае результат метода fail
        /// </summary>
        public static T Return<T>(this Result result, Func<Result, T> success, Func<Result, T> fail)
        {
            return result.Failure ? fail(result) : success(result);
        }

        public static Task<T> Return<T>(this Result result, Func<Result, Task<T>> success, Func<Result, Task<T>> failure)
        {
            return result.Failure ? failure(result) : success(result);
        }

        public static Task<T> Return<T>(this Result result, Func<Task<T>> success, Func<Result, Task<T>> failure)
        {
            if (result.Failure)
            {
                return failure(result);
            }

            return success();
        }

        public static string BuildErrorMessage(this Result src)
        {
            if (src.Errors == null || !src.Errors.Any())
            {
                return string.Empty;
            }

            return string.Join(',', src.Errors.Select(x => x.Message));
        }

        public static Result ThrowIfHasFail<T>(this Result src)
            where T : Exception
        {
            if (src.Success)
            {
                return src;
            }

            var args = new object[] { src.BuildErrorMessage() };
            if (Activator.CreateInstance(typeof(T), args) is T exception)
            {
                throw exception;
            }

            throw new ArgumentNullException(nameof(exception));
        }

        public static async Task<Result> ThrowIfHasFail<T>(this Task<Result> src)
            where T : Exception
        {
            return ThrowIfHasFail<T>(await src);
        }
    }
}