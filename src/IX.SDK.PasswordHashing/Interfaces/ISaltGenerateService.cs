﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IX.SDK.PasswordHashing.Interfaces
{
    public interface ISaltGenerateService
    {
        byte[] Generate(int saltSize);

        byte[] ModifyForMinSize(byte[] salt);
    }
}
