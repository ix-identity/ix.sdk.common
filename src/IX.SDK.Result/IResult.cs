﻿using System;

namespace IX.SDK.Result
{
    public interface IResult
    {
        public bool Success { get; }
        
        public Error[] Errors { get; }
    }
    
    public interface IResult<T> : IResult
    {
        public T Data { get; }
    }
}