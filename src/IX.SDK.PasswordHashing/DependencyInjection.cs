﻿using System;
using System.Reflection;
using System.Security.Cryptography;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace IX.SDK.PasswordHashing
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPasswordHashingService(this IServiceCollection services)
        {
            return AddPasswordHashingService(services, new PasswordHashingServiceOptions());
        }

        public static IServiceCollection AddPasswordHashingService(this IServiceCollection services, Action<PasswordHashingServiceOptions> optionsAction)
        {
            var options = new PasswordHashingServiceOptions();
            optionsAction(options);

            return AddPasswordHashingService(services, options);
        }

        private static IServiceCollection AddPasswordHashingService(IServiceCollection services, PasswordHashingServiceOptions options)
        {
            var saltGeneratedService = new SaltGenerateService(new RNGCryptoServiceProvider());

            services.AddSingleton<IPasswordHashingService>(new PasswordHashingService(options, saltGeneratedService));

            return services;
        }
    }
}
