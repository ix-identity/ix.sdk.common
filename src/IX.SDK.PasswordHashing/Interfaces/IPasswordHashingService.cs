﻿using System;

namespace IX.SDK.PasswordHashing
{
    public interface IPasswordHashingService
    {
        (string Salt, string PasswordHash) Hash(string password);

        string Hash(string password, string salt);

        string Hash(string password, string salt1, string salt2);
    }
}
